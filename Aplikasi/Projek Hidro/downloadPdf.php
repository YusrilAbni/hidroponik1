<?php

  session_start();

	require 'konek2.php';

  require 'tcpdf/tcpdf.php';



  $pdf = new TCPDF('P', PDF_UNIT, "A4", true, 'UTF-8', false);

  $pdf->SetMargins(20, 20, 20, 20);

  $pdf->SetAutoPageBreak(TRUE, 20);

  $pdf->SetFont('Times', '', 12);

  $pdf->SetPrintHeader(false);

  $pdf->SetAuthor('Data Hasil Generate');

  $pdf->SetTitle('PCTH');

  $pdf->AddPage();

  ob_start();

?>

    <style>

        .bold{ font-weight: bold; }

        .pt12{ font-size: 12pt; }

        .pt14{ font-size: 14pt; }

        .pt16{ font-size: 16pt; }

        .pt18{ font-size: 18pt; }

        .center{ text-align: center;}

    </style>





    <p class="pt18 center">Pengaturan Cahaya Terhadap Hidroponik <br/>

    </p>



    <p class="pt16 center">



    </p>

    <p class="pt16 center">



    </p>

    <p class="pt16 center">



    </p>

    <p class="pt16 center">



    </p>

    <p class="pt16 center">






    <p class="pt16 bold center">PCTH</p>

    <p class="pt14 center">Kelompok 1 P2</p>



    </p>

    <p class="pt16 center">



    </p>

    <p class="pt16 center">



    </p>

    <p class="pt16 center">



    <p class="pt18 center">

    Pemrograman Berbasis Web <br/>

    Institut Pertanian Bogor <br/>

    2019

    </p>

    <?php


    require 'konek2.php';

    $sql_tabel = "SELECT `namaTanam`,`warna_merah`,`warna_hijau`,`warna_biru`,`status` ,`tanggal` FROM `tbl_status`";

    $hasil = $conn->query($sql_tabel);

    ?>


<br></br>
  <h2> Pengaturan Cahaya Terhadap Hidroponik </h2>

  <table class="pt12" border="1">

  <tr>
     <th width="1cm">No</th>
     <th>Warna Merah</th>
     <th>Warna Hijau</th>
     <th>Warna Biru</th>
     <th>Status</th>
     <th width="3cm">Tanggal dan Waktu</th>

  </tr>

  <?php

  $no = 1;

  while($row=$hasil->fetch_assoc())

  {

     echo "<tr>";

     echo "<td>".$no."</td>";
     echo "<td>".$row['warna_merah']."R</td>";

     echo "<td>".$row['warna_hijau']."G</td>";

     echo "<td>".$row['warna_biru']."B</td>";

      echo "<td>".$row['status']."</td>";

     echo "<td>".$row['tanggal']."</td>";

     echo "</tr>";

   $no ++;

  }

  ?>


  <?php

	$content = ob_get_contents();

	ob_end_clean();

	$pdf->writeHTML($content, true, false, true, false, '');

	$pdf->Output("PCTH.pdf", 'I');

?>

