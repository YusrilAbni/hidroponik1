-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2019 at 04:18 PM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbhidroponik`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_panen`
--

CREATE TABLE `tbl_panen` (
  `id_panen` int(11) NOT NULL,
  `id_tanam` int(11) NOT NULL,
  `waktu_panen` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_panen`
--

INSERT INTO `tbl_panen` (`id_panen`, `id_tanam`, `waktu_panen`) VALUES
(2, 7, '2019-11-02'),
(3, 7, '2019-11-21'),
(4, 7, '2019-11-02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `id_status` int(11) NOT NULL,
  `namaTanam` varchar(15) NOT NULL,
  `warna_merah` int(11) NOT NULL,
  `warna_hijau` int(11) NOT NULL,
  `warna_biru` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` (`id_status`, `namaTanam`, `warna_merah`, `warna_hijau`, `warna_biru`, `status`, `tanggal`) VALUES
(17, 'Tanaman', 117, 68, 54, 'Kuning', '2019-11-12 11:35:25'),
(19, 'Tanaman', 120, 77, 58, 'Kuning', '2019-11-12 11:35:25'),
(25, 'Tanaman', 123, 67, 24, 'Kuning', '2019-11-12 11:39:36'),
(26, 'saasasas', 103, 1, 73, 'Hijau', '2019-11-12 12:08:59'),
(27, 'Kangkung', 120, 39, 0, 'Kuning', '2019-11-12 12:09:34'),
(28, 'Tanaman', 111, 96, 1, 'Kuning', '2019-11-12 15:38:15'),
(29, 'Tanaman', 104, 211, 52, 'Hijau', '2019-11-12 18:39:25'),
(30, 'nam', 172, 208, 69, 'Hijau', '2019-11-12 18:41:03'),
(31, 'asaasa', 164, 202, 92, 'Hijau', '2019-11-12 18:45:50'),
(32, 'asaasa', 191, 201, 35, 'Hijau', '2019-11-12 18:48:44');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tanaman`
--

CREATE TABLE `tbl_tanaman` (
  `idTanaman` int(11) NOT NULL,
  `namaTanaman` varchar(15) NOT NULL,
  `jmlTanaman` varchar(11) NOT NULL,
  `usia` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_tanaman`
--

INSERT INTO `tbl_tanaman` (`idTanaman`, `namaTanaman`, `jmlTanaman`, `usia`) VALUES
(7, 'kangkung', '3', '1 minggu'),
(11, 'Kacang Ijo', '1', '2 Minggu');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_User` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(20) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_User`, `username`, `password`, `email`, `level`) VALUES
(12, 'yusril', '$2y$10$k06UgMgkaZTh7rYYDIDNa.3z1V8e7vRXXaGb/OFWCab01XyuYDNzq', 'yusril8877@gmail.com', 'admin'),
(15, 'sava', '$2y$10$HwDNLG2UzJGVUesoxz6Ifu/HOrT6c2I6TDNqZqhcTwGNE.EWc5ya.', 'sahirasava@gmail.com', 'admin'),
(16, 'kelompok15', '$2y$10$6WF61kDNFaR597lCRAKvCevQJf/AgRrdvKsPOgNdSH59eEuai5gma', 'dedi@gmail.com', 'user'),
(17, 'novky', '$2y$10$1mEEQ0wnvZWOImqg8Dig8e46in4s2zYEdRYkXH8Odcn9WVX.CDpw.', 'novky@gmail.com', 'user'),
(18, 'susu', '$2y$10$Iy0.UnBoB7xPjP6973lmLuF3IYdUD/IjD9IAEQfIEGxIOSwc4Ol76', 'zuzuuzzu@gmail.com', 'user'),
(20, 'yusrilabni32', '$2y$10$rp9RYl4BYSoqFM3mqmHJFeEpo2PulocwQ6gnyYIg9j.CT8zaXhQSO', 'yusril887789@gmail.c', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_panen`
--
ALTER TABLE `tbl_panen`
  ADD PRIMARY KEY (`id_panen`),
  ADD KEY `id_tanam` (`id_tanam`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `tbl_tanaman`
--
ALTER TABLE `tbl_tanaman`
  ADD PRIMARY KEY (`idTanaman`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_User`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_panen`
--
ALTER TABLE `tbl_panen`
  MODIFY `id_panen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_status`
--
ALTER TABLE `tbl_status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tbl_tanaman`
--
ALTER TABLE `tbl_tanaman`
  MODIFY `idTanaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_User` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_panen`
--
ALTER TABLE `tbl_panen`
  ADD CONSTRAINT `tbl_panen_ibfk_1` FOREIGN KEY (`id_tanam`) REFERENCES `tbl_tanaman` (`idTanaman`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
