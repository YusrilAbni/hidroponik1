<?php
require 'konek.php';
$sql = "SELECT `namaTanam`,`warna_merah`,`warna_hijau`,`warna_biru`, `tanggal` FROM `tbl_status` ORDER BY `tanggal` DESC LIMIT 10"; 
    $result = $conn->query($sql);
    $kategori = array();
    $warna_merah = array();
    $warna_hijau = array();
    $warna_biru = array();
    $tanggal = array();

    while($rc = mysqli_fetch_assoc($result)){
  		array_unshift($kategori,array("label"=>$rc["tanggal"]));
  		array_unshift($warna_merah,array("value"=>$rc["warna_merah"]));
  		array_unshift($warna_hijau,array("value"=>$rc["warna_hijau"]));
  		array_unshift($warna_biru,array("value"=>$rc["warna_biru"]));
   	}

    $kategori = json_encode($kategori);
    $warna_merah = json_encode($warna_merah);
    $warna_hijau = json_encode($warna_hijau);
    $warna_biru = json_encode($warna_biru);
    ?>
    <!DOCTYPE html>
    <html>
    <head>
      <script type="text/javascript" src="script.js"></script>

      <!--<script type="text/javascript" src="script_fussion.js"></script>
      <script type="text/javascript" src="pustaka_FC/js/fusioncharts.js"></script>
      <script type="text/javascript" src="pustaka_FC/js/themes/fusioncharts.theme.fint.js"></script>-->
      <script src="https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
      <script src="https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
      <script type="text/javascript">
      FusionCharts.ready(function() {
            var visitChart = new FusionCharts({
              type: 'msline',
              renderAt: 'chart-container',
              width: '900',
              height: '400',
              dataFormat: 'json',
              dataSource: {
                "chart": {"theme": "fusion","caption": "data Grafik Pengujian Tanaman","xAxisName": "Value"},
                "categories": [{"category": <?php echo $kategori; ?>}],
                "dataset": [
                {"seriesname": "Warna_biru","data": <?php echo $warna_biru; ?>},
                {"seriesname": "Warna_hijau","data": <?php echo $warna_hijau; ?>},
                {"seriesname": "Warna_merah","data": <?php echo $warna_merah;  ?>},
                ],
                "trendlines": [{
                  "line": [{
                    "color": "#62B58F",
                    "valueOnRight": "1"
                  }]
                }]
              }
            }).render();
          });
      </script>
      <title>grafik hidro</title>
      <center><section class="white" id="section4">
          <div id="chart-container">FusionCharts will render here</div>
          <br/>
          <center><a style="background-color: #006d9f; color: white; padding:10px 0px;width: 200px; text-align: center; text-decoration: none; display: inline-block;" href="downloadPdf.php">Download Pdf</a>
            <a style="background-color: #006d9f; color: white; padding:10px 0px;width: 200px; text-align: center; text-decoration: none; display: inline-block;" href="downloadXlsx.php">Download Excel</a></center>
    </section></center>
    </head>
    <body>
    
    </body>
    </html>